package br.ucsal.poo.bes20192.avaliacao01.t1.domain;

import java.util.List;
import java.util.ArrayList;

public class Motorista {

	private Integer numCarteira;

	private String nome;

	private List<Multa> multas = new ArrayList<>();

	public Motorista(Integer numCarteira, String nome) {
		super();
		this.numCarteira = numCarteira;
		this.nome = nome;
	}

	public Integer getNumCarteira() {
		return numCarteira;
	}

	public void setNumCarteira(Integer numCarteira) {
		this.numCarteira = numCarteira;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Multa> getMultas() {
		return multas;
	}

	public void setMultas(Multa multa) {
		multas.add(multa);
	}
}