package br.ucsal.poo.bes20192.avaliacao01.t1.domain;

public class Multa {
	
	private String local;
	
	private String descricao;
	
	private CategoriaMultaEnum categoria;

	public Multa(String local, String descricao, CategoriaMultaEnum categoria) {
		super();
		this.local = local;
		this.descricao = descricao;
		this.categoria = categoria;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public CategoriaMultaEnum getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaMultaEnum categoria) {
		this.categoria = categoria;
	}
}