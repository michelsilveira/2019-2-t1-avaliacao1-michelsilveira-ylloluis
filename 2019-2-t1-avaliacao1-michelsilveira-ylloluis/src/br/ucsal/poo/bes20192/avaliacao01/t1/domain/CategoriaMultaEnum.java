package br.ucsal.poo.bes20192.avaliacao01.t1.domain;

public enum CategoriaMultaEnum {

	LEVE(3), MEDIA(4), GRAVE(5), GRAVISSIMA(7);

	Integer categoria;

	private CategoriaMultaEnum(Integer categoria) {
		this.categoria = categoria;
	}
}