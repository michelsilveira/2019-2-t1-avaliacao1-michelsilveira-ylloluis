package br.ucsal.poo.bes20192.avaliacao01.t1.business;

import br.ucsal.poo.bes20192.avaliacao01.t1.domain.Motorista;
import br.ucsal.poo.bes20192.avaliacao01.t1.domain.MotoristaCarga;
import br.ucsal.poo.bes20192.avaliacao01.t1.domain.CategoriaMultaEnum;
import br.ucsal.poo.bes20192.avaliacao01.t1.domain.Multa;
import br.ucsal.poo.bes20192.avaliacao01.t1.persistance.MotoristaDAO;

public class MotoristaBO {

	public static void cadastrarMotorista(Integer numCarteira, String nome) {
		Motorista motorista = new Motorista(numCarteira, nome);
		MotoristaDAO.salvar(motorista);
	}

	public static void cadastrarMotoristaCarga(Integer numCarteira, String nome, Integer anoToxicologico) {
		MotoristaCarga motoristaCarga = new MotoristaCarga(numCarteira, nome, anoToxicologico);
		MotoristaDAO.salvar(motoristaCarga);
	}

	public static void adicionarMulta(Integer numCarteira, String local, String descricao,
			CategoriaMultaEnum categoria) {
		Motorista motoristaMultado = MotoristaDAO.pesquisar(numCarteira);
		Multa multa = new Multa(local, descricao, categoria);
		motoristaMultado.setMultas(multa);
	}
}
