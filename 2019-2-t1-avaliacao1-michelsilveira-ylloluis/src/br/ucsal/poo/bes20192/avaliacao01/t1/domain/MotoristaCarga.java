package br.ucsal.poo.bes20192.avaliacao01.t1.domain;

public class MotoristaCarga extends Motorista {

	private Integer anoToxicologico;

	public MotoristaCarga(Integer numCarteira, String nome, Integer anoToxicologico) {
		super(numCarteira, nome);
		this.anoToxicologico = anoToxicologico;
	}

	public Integer getAnoToxicologico() {
		return anoToxicologico;
	}

	public void setAnoToxicologico(Integer anoToxicologico) {
		this.anoToxicologico = anoToxicologico;
	}
}