package br.ucsal.poo.bes20192.avaliacao01.t1.persistance;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.poo.bes20192.avaliacao01.t1.domain.Motorista;

public class MotoristaDAO {

	static List<Motorista> motoristas = new ArrayList<>();

	public static void salvar(Motorista motorista) {
		motoristas.add(motorista);
	}

	public static Motorista pesquisar(Integer numCarteira) {
		for (Motorista motorista : motoristas) {
			if (motorista.getNumCarteira().equals(numCarteira)) {
				return motorista;
			}
		}
		return null;
	}
}